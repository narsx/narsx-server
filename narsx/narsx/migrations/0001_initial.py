# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(blank=True, max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Invitation',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('login', models.CharField(max_length=32)),
                ('ip_address', models.GenericIPAddressField(null=True)),
                ('port', models.IntegerField(null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='invitation',
            name='collaborator',
            field=models.ForeignKey(related_name='invited_collaborators', to='narsx.User'),
        ),
        migrations.AddField(
            model_name='invitation',
            name='document',
            field=models.ForeignKey(related_name='shared_documents', to='narsx.Document'),
        ),
        migrations.AddField(
            model_name='document',
            name='author',
            field=models.ForeignKey(related_name='documents', to='narsx.User'),
        ),
        migrations.AddField(
            model_name='document',
            name='collaborators',
            field=models.ManyToManyField(through='narsx.Invitation', to='narsx.User'),
        ),
    ]
