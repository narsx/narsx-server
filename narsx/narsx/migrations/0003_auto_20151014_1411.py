# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('narsx', '0002_remove_user_login'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='ip_address',
            field=models.GenericIPAddressField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='port',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
