# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('narsx', '0003_auto_20151014_1411'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='ip_address',
        ),
        migrations.RemoveField(
            model_name='user',
            name='port',
        ),
        migrations.AddField(
            model_name='user',
            name='token',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
