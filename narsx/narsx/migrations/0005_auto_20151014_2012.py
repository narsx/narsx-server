# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('narsx', '0004_auto_20151014_1951'),
    ]

    operations = [
        migrations.CreateModel(
            name='Editor',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('token', models.CharField(null=True, blank=True, max_length=255)),
            ],
        ),
        migrations.RemoveField(
            model_name='user',
            name='user',
        ),
        migrations.AlterField(
            model_name='document',
            name='author',
            field=models.ForeignKey(related_name='documents', to='narsx.Editor'),
        ),
        migrations.AlterField(
            model_name='document',
            name='collaborators',
            field=models.ManyToManyField(to='narsx.Editor', through='narsx.Invitation'),
        ),
        migrations.AlterField(
            model_name='invitation',
            name='collaborator',
            field=models.ForeignKey(related_name='invited_collaborators', to='narsx.Editor'),
        ),
        migrations.DeleteModel(
            name='User',
        ),
        migrations.AddField(
            model_name='editor',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
