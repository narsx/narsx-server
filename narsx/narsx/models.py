"""
    Narsx server is meant to serve as a back office for Narsx: the
    connected coding solution.

    Copyright (C) 2015  Narsx-Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'jaune'

from django.db import models
from django.contrib.auth.models import User


class Editor(models.Model):
    token = models.CharField(max_length=255, null=True, blank=True)
    user = models.ForeignKey(User)

    @classmethod
    def get_all_usernames(cls):
        return [e.user.username for e in Editor.objects.all()]

    def documents_shared_with_me(self):
        return Document.shared_with(self)

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.__unicode__()


class Document(models.Model):
    title = models.CharField(max_length=255, blank=True, null=False, unique=True)
    author = models.ForeignKey('Editor', related_name='documents')
    collaborators = models.ManyToManyField(Editor, through='Invitation', through_fields=('document', 'collaborator'))

    @classmethod
    def shared_with(cls, editor):
        return [doc for doc in Document.objects.all() if editor in doc.collaborators.all()]

    def is_shared_with(self, editor):
        return editor in self.collaborators.all()

    def share_with(self, editor):
        Invitation(collaborator=editor, document=self).save()

    def __unicode__(self):
        return self.title + ' [' + self.author.user.username + ']'

    def __str__(self):
        return self.__unicode__()


class Invitation(models.Model):
    document = models.ForeignKey(Document, related_name='shared_documents')
    collaborator = models.ForeignKey(Editor, related_name='invited_collaborators')

    def __unicode__(self):
        return str(self.document) + " shared with " + str(self.collaborator)

    def __str__(self):
        return self.__unicode__()