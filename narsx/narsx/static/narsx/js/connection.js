const PEERJS_ID = 'sdltet2jek0ara4i';

const UPDATE_TIMEOUT = 1000;

const SYNC = "SYNC";
const RSYNC = "RSYNC";
const UPDATE = "UPDATE";
const LOCK = "LOCK";

/*****************************************
 *
 * CONNECTION HANDLER
 *
 ****************************************/

/**
 * Ctor for connection handler.
 * @param onUpdateCallback the function to call when the connector receives an UPDATE packet.
 * @constructor
 */
function ConnectionHandler(onUpdateCallback) {
    this.peer = ConnectionHandler._getNewPeerId();
    this.onUpdateCallback = onUpdateCallback;
    this.connection = undefined;
}

/**
 * Reset the timeout before the next update to send.
 * @param documentId the current document (CodeMirror id)
 * @param jsonPatch the patch to be sent
 * @param masterVersion the master version number known to the connector
 * @param clientVersion the client version number known to the connector
 */
ConnectionHandler.prototype.resetUpdateTimeout = function(documentId, jsonPatch, masterVersion, clientVersion) {
    setTimeout(
        ConnectionHandler.prototype.sendUpdate,
        UPDATE_TIMEOUT,
        this,
        documentId, jsonPatch, masterVersion, clientVersion
    )
};

/**
 * Request a peer token to PeerJS server
 * @returns a Peer connection object with the requested token
 * @private
 */
ConnectionHandler._getNewPeerId = function () {
    return new Peer({key: PEERJS_ID});
};

/**
 * Receive a PeerJS connection error and print/alert the corresponding error message
 * @param err the raised error
 * @private
 */
ConnectionHandler.prototype._treatError = function (err) {
    switch (err.type.toUpperCase()) {
        case 'BROWSER-INCOMPATIBLE':
            console.log('The client\'s browser does not support some or all WebRTC features that you are trying to use.', err);
            alert('The client\'s browser does not support some or all WebRTC features that you are trying to use.');
            break;
        case 'DISCONNECTED':
            console.log('You\'ve already disconnected this peer from the server and can no longer make any new connections on it.', err);
            alert('You\'ve already disconnected this peer from the server and can no longer make any new connections on it.');
            break;
        case 'INVALID-ID':
            console.log('The ID passed into the Peer constructor contains illegal characters.', err);
            alert('Invalid Peer ID. Please contact the administrator.');
            break;
        case 'INVALID-KEY':
            console.log('The API key is wrong.', err);
            alert('The API key is wrong. Please contact your administrator.');
            break;
        case 'NETWORK':
            console.log('Lost or cannot establish a connection.', err);
            alert('Lost or cannot establish a connection.');
            break;
        case 'PEER-UNAVAILABLE':
            console.log('The peer you\'re trying to connect to does not exist', err);
            alert('The peer you\'re trying to connect to does not exist');
            break;
        case 'SERVER-ERROR':
            console.log('Unable to reach the server.', err);
            alert('Unable to reach the server. Please contact your administrator.');
            break;
        case 'SOCKET-ERROR':
            console.log('An error from the underlying error', err);
            alert('There has been an error. Please contact your administrator.');
            break;
        case 'SOCKET-CLOSED':
            console.log('The underlying socket closed unexpectedly.', err);
            alert('The connection has been lost.');
            break;
        default:
            console.log('ERROR not referenced', err);
            break;
    }
};

/**
 * Receive a packet and call the appropriate function, depending on packet type.
 * @param data the received packet
 * @private
 */
ConnectionHandler.prototype._receiveData = function(data) {
    switch (data.typePackage) {
        case RSYNC:
            if (typeof this.onRsyncCallback !== "undefined")
                this.onRsyncCallback(this, data);
            break;
        case SYNC:
            if (typeof this.onSyncCallback !== "undefined")
                this.onSyncCallback(this, data);
            break;
        case UPDATE:
            this.onUpdateCallback(this, data);
            break;
        case LOCK:
            break;
        default:
            break;
    }
};

/**
 * Send an UPDATE packet
 * @param connector the connector object to send the packet from.
 * @param documentId the id of the document (CodeMirror ID)
 * @param jsonPatch the diff patch to be sent
 * @param masterV the master version number known to the connector
 * @param clientV the client version number known to the connector
 */
ConnectionHandler.prototype.sendUpdate = function(connector, documentId, jsonPatch, masterV, clientV) {
    var res = {
        idDoc: documentId,
        typePackage: UPDATE,
        patch: jsonPatch,
        masterVersion: masterV,
        clientVersion: clientV
    };

    var cache = [];
    var json = JSON.stringify(res, function(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                return;
            }
            // Store value in our collection
            cache.push(value);
        }
        return value;
    });
    cache = null; // Enable garbage collection

    connector.connection.send(json);
};

/*****************************************
 *
 * CLIENT CONNECTION HANDLER
 *
 ****************************************/

/**
 * Connection Handler for a Client (collaborator, as opposed the Master/author).
 *
 * @param authorToken the token of the author to connect to
 * @param onRsyncCallback the function to call when a RSYNC packet is received
 * @param onUpdateCallback the function to call when an UPDATE packet is received
 * @constructor
 */
function ClientConnectionHandler(authorToken, onRsyncCallback, onUpdateCallback) {
    ConnectionHandler.call(this, onUpdateCallback);
    this.connection = this.peer.connect(authorToken);

    this.onRsyncCallback = onRsyncCallback;
    var self = this;

    this.connection.on('open', function () {
        self.sendSyncRequest(0);
        self.connection.on('data', function (data) {
            self._receiveData(JSON.parse(data));
        });

        self.connection.on('error', function (err) {
            self._treatError(err);
        });
    });
}

ClientConnectionHandler.prototype = ConnectionHandler.prototype;

/**
 * Send a SYNC packet to the Master, requesting their full copy of the text.
 * @param documentId the id of the edited document (CodeMirror ID)
 */
ClientConnectionHandler.prototype.sendSyncRequest = function(documentId) {
    var json = JSON.stringify({
        idDoc: documentId,
        typePackage: SYNC
    });

    this.connection.send(json);
};

/*****************************************
 *
 * MASTER CONNECTION HANDLER
 *
 ****************************************/

/**
 *
 * @param registerTokenFunction the function to call to register the token in order to make it
 * accessible to clients.
 * @param onSyncCallback the function to call when receiving a SYNC packet
 * @param onUpdateCallback the function to call when receiving an UPDATE packet
 * @constructor
 */
function MasterConnectionHandler(registerTokenFunction, onSyncCallback, onUpdateCallback) {
    ConnectionHandler.call(this, onUpdateCallback);

    this.onSyncCallback = onSyncCallback;
    var self = this;

    this.peer.on('open', function(token) {
        self._registerToken(token, registerTokenFunction)
    });

    this.peer.on('connection', function (connection) {
        console.log("A peer has connected");
        self.connection = connection;

        self.connection.on('open', function (token) {
            self.connection.on('data', function (data) {
                self._receiveData(JSON.parse(data));
            });

            self.connection.on('error', function (err) {
                self._treatError(err);
            });
        });
    });
}

MasterConnectionHandler.prototype = ConnectionHandler.prototype;

/**
 * Register the PeerJS token through the given function (in order to make it accessible to clients)
 * @param token the token to register
 * @param registerFunction(token, successCB, errorCB) the function to call in order to register the token
 * @private
 */
MasterConnectionHandler.prototype._registerToken = function (token, registerFunction) {
    registerFunction(token, function () {
            console.log("Peer token has been saved to the DB");
        },
        function () {
            console.log("Error while contacting the DB. Peer token could not be saved.");
            alert("There has been a problem: other users may not be able to join you");
        });
};

/**
 * Send an RSYNC packet with the full edited text to resync the client.
 * @param documentId the CodeMirror id of the document
 * @param editedText the full text on master's side
 */
MasterConnectionHandler.prototype.sendSyncAnswer = function(documentId, editedText) {
    var json = JSON.stringify({
        idDoc: documentId,
        typePackage: RSYNC,
        text: editedText
    });

    this.connection.send(json);
};