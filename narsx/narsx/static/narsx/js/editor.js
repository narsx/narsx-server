/**
 * Ctor for Editor
 * @param isMaster whether the editor to create is a Master or a Slave
 * @constructor
 */
function Editor(isMaster) {
    this.isMaster = isMaster;
    this.shadow = {
        text: "",
        masterVersion: 0,
        clientVersion: 0
    };

    this.backup = this.isMaster ? {
        text: "",
        masterVersion: 0
    } : null;

    this.stack = !this.isMaster ? [] : null;

    this._patcher = new diff_match_patch();
}

/**
 * Initialize all version information and shadow/backup copies before
 * sending the full edited text for a resync'. (Master only)
 *
 * @param text the edited text to send.
 */
Editor.prototype.prepareSync = function(text) {
    if (!this.isMaster)
        return;

    this.shadow.masterVersion = 0;
    this.shadow.clientVersion = 0;

    this.shadow.text = text;
    this.backup.text = text;
    this.backup.masterVersion = 0;
};

/**
 * Apply the request resync' to the shadow copy (Client only)
 *
 * @param text the sync'ed text
 */
Editor.prototype.receiveSync = function(text) {
    if (this.isMaster)
        return;

    this.shadow.text = text;
};

/**
 * Increment the version indexes, computes the text/shadow diff patch and updates the shadow.
 *
 * @param text the edited text
 * @return the text/shadow diff patch in JSON format
 */
Editor.prototype.prepareUpdate = function(text) {
    var patch;
    if (this.isMaster) {
        this.shadow.masterVersion++;
        patch = this._getJSONDiffWithText(text);

    } else {
        this.shadow.clientVersion++;
        this.stack.push(this._getJSONDiffWithText(text));
        patch = JSON.stringify(this.stack);
    }
    
    this.shadow.text = text;

    return patch;
};

/**
 * Treat a received update by patching the shadow and edited text, according to
 * the received version number.
 *
 * @param patch the received patch to apply
 * @param clientVersion the other party's client version
 * @param masterVersion the other party's master version
 */
Editor.prototype.receiveUpdate = function(patch, clientVersion, masterVersion) {
    if (this.isMaster) {
        if (masterVersion != this.shadow.masterVersion) {
            this.shadow.text = this.backup.text;
            this.shadow.masterVersion = this.backup.masterVersion;
        }

        if (clientVersion != this.shadow.clientVersion) {
            for (var i = 0; i < patch.length; i++){
                var p = patch[i];
                p = p.replace('{{39}}', "'");
                p = p.replace('\\\\', '\\');
                this._patchShadow(JSON.parse(p));
                this.shadow.clientVersion++;
                this.backup.text = this.shadow.text;
                this.backup.masterVersion = this.shadow.masterVersion;
            }
        }
    } else {
        if (clientVersion == this.shadow.clientVersion) {
            this.stack = [];
        } else {
            var stackToKeep = this.shadow.clientVersion - clientVersion;
            while (this.stack.length != stackToKeep){
                this.stack.pop();
            }
        }

        this._patchShadow(patch);
        this.shadow.masterVersion++;
    }
};



/**
 * Apply a patch to shadow.
 * @param patch the patch to apply
 * @private
 */
Editor.prototype._patchShadow = function(patch) {
    this.shadow.text = this._patcher.patch_apply(patch, this.shadow.text)[0];
};

/**
 * Compute the diff patch between the shadow text and a given text
 * then return the patch as JSON
 *
 * @param text the text to diff with the shadow.
 * @private
 */
Editor.prototype._getJSONDiffWithText = function(text) {
    var patch = JSON.stringify(
        this._patcher.patch_make(this.shadow.text, text)
    );

    patch = patch.replace('\\', '\\\\');
    patch = patch.replace("'", "{{39}}");

    return patch;
};
