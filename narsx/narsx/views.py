"""
    Narsx server is meant to serve as a back office for Narsx: the
    connected coding solution.

    Copyright (C) 2015  Narsx-Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
from django.http import HttpResponse

__author__ = 'jaune'

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as django_login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from .models import Document, Editor


@login_required
def index(request):
    editor = get_request_editor(request)

    return render(request, 'narsx/index.html', {'user': editor})


@login_required
def new(request):
    editor = get_request_editor(request)

    document = Document(
        author=editor,
        title=request.GET['title']
    )
    document.save()

    try:
        doc = Document.objects.get(title=request.GET['title'])
    except Document.DoesNotExist:
        return redirect(index)

    return redirect('/show/' + str(doc.id))


@login_required
def show(request, document_id):
    editor = get_request_editor(request)

    try:
        document = Document.objects.get(pk=document_id)
        if not document.shared_with(editor) and not document.author == editor:
            return redirect(index)
    except Document.DoesNotExist:
        return redirect(index)

    return render(request, 'narsx/document.html', {'document': document, 'user': editor})


@login_required
def set_token(request):
    if not request.GET or not request.GET['token']:
        return HttpResponse(status=400)

    editor = get_request_editor(request)
    if editor is None:
        return HttpResponse(status=400)

    editor.token = request.GET['token']
    editor.save()

    if get_request_editor(request).token == request.GET['token']:
        return HttpResponse(json.dumps({'status': 'ok'}))
    else:
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Error saving token'}))


def user_list(request):
    return HttpResponse(json.dumps({'users': Editor.get_all_usernames()}))


@login_required
def invite(request, document_id):
    if not request.POST or not request.POST['invitee']:
        print("no POST")
        return HttpResponse(status=400)

    try:
        document = Document.objects.get(pk=document_id)
        invitee_user = User.objects.get(username=request.POST['invitee'])
        invitee = Editor.objects.get(user=invitee_user)
    except Document.DoesNotExist:
        print("Doc not exist")
        return HttpResponse(status=400)
    except User.DoesNotExist:
        print("User not exist")
        return HttpResponse(json.dumps({'status': 'error', 'message': 'The user does not exist.'}))
    except Editor.DoesNotExist:
        print("Editor not exist")
        return HttpResponse(json.dumps({'status': 'error', 'message': 'The user does not exist.'}))

    user = get_request_editor(request)

    if document.author == user and user is not None:
        document.share_with(invitee)
        print("ok")
        return HttpResponse(json.dumps({'status': 'ok'}))
    else:
        print("no auth")
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Unauthorized document'}))


def login(request):
    return render(request, 'narsx/login.html')


def signin(request):
    if not request.POST:
        return redirect(login)

    username = request.POST['login']
    password = request.POST['password']

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            django_login(request, user)
            try:
                Editor.objects.get(user=request.user)
            except Editor.DoesNotExist:
                return redirect(login)

            return redirect(index)
        else:
            return redirect(login)
    else:
        return redirect(login)


@login_required
def signout(request):
    logout(request)
    return redirect(login)


def get_request_editor(request):
    try:
        return Editor.objects.get(user=request.user)
    except Editor.DoesNotExist:
        return None


def contactus(request):
        return render(request, 'narsx/contactus.html')

